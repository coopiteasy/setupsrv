#!/bin/bash

# Quit if a command fails
set -e

# This working directory should correspond to the root where this script
# stand.
WD=$(dirname "$(realpath -s "$0")")

_ubuntuver=$(lsb_release --codename --short)

# Install certbot
if [ "${_ubuntuver}" = "bionic" ]; then
    add-apt-repository --yes ppa:certbot/certbot
    apt update
    apt install --yes certbot
elif ["${_ubuntuver}" = "focal" ]; then
    apt install --yes certbot
fi

# Install nginx
apt install --yes nginx

# Configure nginx
cp "$WD/files/nginx-cie.conf" /etc/nginx/conf.d/

# Activate certificate renewal
echo "# Certificate renewal
0 0 * * 0 root /usr/ciebin/certrenew >> /var/log/certrenew.log 2>&1" \
    > /etc/cron.d/certrenew
