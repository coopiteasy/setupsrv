setupsrv
========

Prerequisites
-------------

**Warning** Order is important !

- Install "Ubuntu Server" via control panel of the cloud provider.
- If root is not accessible, enable it by giving it a password:
    - `sudo passwd root`
    - Change `PermitRootLogin password-prohibited` by
      `PermitRootLogin yes` in `/etc/ssh/sshd_config`
    - Reload sshd with: `systemctl reload ssh`
- Execute the following instructions on [Odoo 12.0 Linux Server Installation](https://gitlab.com/coopiteasy/doc/-/blob/master/documentation/install-odoo-12-linux-server.md):
    - change `root` password (if not already done before).
    - [Baptême](https://gitlab.com/coopiteasy/doc/-/blob/master/documentation/install-odoo-12-linux-server.md#bapt%C3%AAme)
    - add `ssh` keys
- Clone `https://gitlab.com/coopiteasy/setupsrv`
- Run: `apt update`


Scripts
-------

Scripts should be run, as `root`, in this order:

1. `bash setup-security.sh`
installation and/or configuration for:
- `sshd_config`
- `fail2ban`

2. `bash setup-bash.sh`
installation and/or configuration for:
- `.bashrc`

3. `bash setup-web-server.sh`
installation and/or configuration for:
- `certbot`
- `nginx`
- `certificate renewal`

4. `bash setup-odoo-dependencies.sh`
installation and/or configuration for:
- `postgresql`
- odoo python dependencies devel requirements
- `node-less`
- `wkhtmltopdf`

5. `bash setup-cie-tools.sh`
installation and/or configuration for:
- tools and scripts used by Coop IT Easy

6. `bash prepare-odoo-instance.sh odoo`
installation and/or configuration for:
- odoo instance: `odoo` is the user (e.g. `/home/odoo`)

Connect as `odoo` user, then get its public ssh-key file.
Connect to gitlab with user `infra-cie` (do not add deploy keys with
your own gitlab user !) and add this key as a deploy key with write
permission on the right inventory in
https://gitlab.com/coopiteasy/srv/

Go to other private repositories that need to be accessed via
git-aggregate and enable the new created deploy key to these
repositories.

Connect on github as `infra-cie` and add ssh-key for this user too.

7. as `root` execute
```sh
setup-odoo-instance.sh -r REPOURL -p python3 -e test ODOOINSTANCE
```

8. Use cie-scripts to create a new instance.

9. On OVH VPS, follow
   [this tutorial](https://gitlab.com/coopiteasy/doc/-/blob/master/documentation/activate-ipv6-ovh-vps.md)
   to activate ipv6 address.


Know issues
-----------

### Issue with psycopg2

```
ImportError: /home/odoo9/venv/lib/python2.7/site-packages/psycopg2/.libs/libresolv-2-c4c53def.5.so: undefined symbol: __res_maybe_init, version GLIBC_PRIVATE
```

Fix:
```sh
su - odoo9
source ~/venv/bin/activate
pip install --no-binary :all: --force-reinstall psycopg2==2.7.1
```

### Issue with jinja

```
2021-05-25 21:27:03,517 186340 CRITICAL ? odoo.modules.module: Couldn't load module web
2021-05-25 21:27:03,517 186340 CRITICAL ? odoo.modules.module: odoo.addons.web.__spec__ is None
2021-05-25 21:27:03,517 186340 ERROR ? odoo.service.server: Failed to load server-wide module `web`.
The `web` module is provided by the addons found in the `openerp-web` project.
Maybe you forgot to add those addons in your addons_path configuration.
Traceback (most recent call last):
  File "/home/odoo12/src/odoo/odoo/service/server.py", line 1109, in load_server_wide_modules
    odoo.modules.module.load_openerp_module(m)
  File "/home/odoo12/src/odoo/odoo/modules/module.py", line 368, in load_openerp_module
    __import__('odoo.addons.' + module_name)
  File "<frozen importlib._bootstrap>", line 971, in _find_and_load
  File "<frozen importlib._bootstrap>", line 955, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 656, in _load_unlocked
  File "<frozen importlib._bootstrap>", line 626, in _load_backward_compatible
  File "/home/odoo12/src/odoo/odoo/modules/module.py", line 82, in load_module
    exec(open(modfile, 'rb').read(), new_mod.__dict__)
  File "<string>", line 4, in <module>
  File "/home/odoo12/src/odoo/addons/web/controllers/__init__.py", line 4, in <module>
    from . import main, pivot
  File "/home/odoo12/src/odoo/addons/web/controllers/main.py", line 58, in <module>
    loader = jinja2.PackageLoader('odoo.addons.web', "views")
  File "/home/odoo12/venv/lib/python3.6/site-packages/jinja2/loaders.py", line 286, in __init__
    spec = importlib.util.find_spec(package_name)
  File "/usr/lib/python3.6/importlib/util.py", line 102, in find_spec
    raise ValueError('{}.__spec__ is None'.format(name))
ValueError: odoo.addons.web.__spec__ is None
```

Fix:
```sh
su - odoo12
source ~/venv/bin/activate
pip install --force-reinstall Jinja2==2.10.1
```


### Missing python dependencies for odoo9

```
2021-05-30 21:05:32,513 870619 ERROR ? werkzeug: Error on request:
Traceback (most recent call last):
  File "/home/odoo9/venv/lib/python2.7/site-packages/werkzeug/serving.py", line 177, in run_wsgi
    execute(self.server.app)
  File "/home/odoo9/venv/lib/python2.7/site-packages/werkzeug/serving.py", line 165, in execute
    application_iter = app(environ, start_response)
  File "/home/odoo9/src/odoo/openerp/service/wsgi_server.py", line 184, in application
    return application_unproxied(environ, start_response)
  File "/home/odoo9/src/odoo/openerp/service/wsgi_server.py", line 170, in application_unproxied
    result = handler(environ, start_response)
  File "/home/odoo9/src/odoo/openerp/http.py", line 1514, in __call__
    self.load_addons()
  File "/home/odoo9/src/odoo/openerp/http.py", line 1535, in load_addons
    m = __import__('openerp.addons.' + module)
  File "/home/odoo9/src/odoo/openerp/modules/module.py", line 61, in load_module
    mod = imp.load_module('openerp.addons.' + module_part, f, path, descr)
  File "/home/odoo9/src/oca/account-reconcile/account_move_bankaccount_import/__init__.py", line 5, in <module>
    from . import models
  File "/home/odoo9/src/oca/account-reconcile/account_move_bankaccount_import/models/__init__.py", line 5, in <module>
    from . import account_move
  File "/home/odoo9/src/oca/account-reconcile/account_move_bankaccount_import/models/account_move.py", line 6, in <module>
    from openerp.addons.account_move_base_import.models.account_move \
  File "/home/odoo9/src/odoo/openerp/modules/module.py", line 61, in load_module
    mod = imp.load_module('openerp.addons.' + module_part, f, path, descr)
  File "/home/odoo9/src/oca/account-reconcile/account_move_base_import/__init__.py", line 7, in <module>
    from . import parser
  File "/home/odoo9/src/oca/account-reconcile/account_move_base_import/parser/__init__.py", line 9, in <module>
    from . import file_parser
  File "/home/odoo9/src/oca/account-reconcile/account_move_base_import/parser/file_parser.py", line 15, in <module>
    raise Exception(_('Please install python lib xlrd'))
Exception: Please install python lib xlrd
```

Fix:
```sh
su - odoo9
source ~/venv/bin/activate
pip install xlrd
```


### Issue with pyotp

```
2021-05-30 21:08:53,518 871054 ERROR ? werkzeug: Error on request:
Traceback (most recent call last):
  File "/home/odoo9/venv/lib/python2.7/site-packages/werkzeug/serving.py", line 177, in run_wsgi
    execute(self.server.app)
  File "/home/odoo9/venv/lib/python2.7/site-packages/werkzeug/serving.py", line 165, in execute
    application_iter = app(environ, start_response)
  File "/home/odoo9/src/odoo/openerp/service/wsgi_server.py", line 184, in application
    return application_unproxied(environ, start_response)
  File "/home/odoo9/src/odoo/openerp/service/wsgi_server.py", line 170, in application_unproxied
    result = handler(environ, start_response)
  File "/home/odoo9/src/odoo/openerp/http.py", line 1514, in __call__
    self.load_addons()
  File "/home/odoo9/src/odoo/openerp/http.py", line 1535, in load_addons
    m = __import__('openerp.addons.' + module)
  File "/home/odoo9/src/odoo/openerp/modules/module.py", line 61, in load_module
    mod = imp.load_module('openerp.addons.' + module_part, f, path, descr)
  File "/home/odoo9/src/oca/server-tools/auth_totp/__init__.py", line 7, in <module>
    from . import models
  File "/home/odoo9/src/oca/server-tools/auth_totp/models/__init__.py", line 6, in <module>
    from . import res_users_authenticator
  File "/home/odoo9/src/oca/server-tools/auth_totp/models/res_users_authenticator.py", line 10, in <module>
    import pyotp
File "/home/odoo9/venv/lib/python2.7/site-packages/pyotp/__init__.py", line 12
    def random_base32(length: int = 32, chars: Sequence[str] = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ234567')) -> str:
                            ^
SyntaxError: invalid syntax
```

Fix:
```sh
su - odoo9
source ~/venv/bin/activate
pip install --force-reinstall pyotp==2.3.0
```


### Issue with python-stdnum

```
2021-06-16 08:40:22,374 2321422 CRITICAL grainesdevie-test odoo.modules.module: Couldn't load module base_vat_autocomplete
2021-06-16 08:40:22,374 2321422 CRITICAL grainesdevie-test odoo.modules.module: module 'stdnum.eu.vat' has no attribute '_country_codes'
2021-06-16 08:40:22,379 2321422 WARNING grainesdevie-test odoo.modules.loading: Transient module states were reset
2021-06-16 08:40:22,380 2321422 ERROR grainesdevie-test odoo.modules.registry: Failed to load registry
Traceback (most recent call last):
  File "/home/odoo11/src/odoo/odoo/modules/registry.py", line 85, in new
    odoo.modules.load_modules(registry._db, force_demo, status, update_module)
  File "/home/odoo11/src/odoo/odoo/modules/loading.py", line 379, in load_modules
    force, status, report, loaded_modules, update_module, models_to_check)
  File "/home/odoo11/src/odoo/odoo/modules/loading.py", line 274, in load_marked_modules
    perform_checks=perform_checks, models_to_check=models_to_check
  File "/home/odoo11/src/odoo/odoo/modules/loading.py", line 137, in load_module_graph
    load_openerp_module(package.name)
  File "/home/odoo11/src/odoo/odoo/modules/module.py", line 368, in load_openerp_module
    __import__('odoo.addons.' + module_name)
  File "<frozen importlib._bootstrap>", line 968, in _find_and_load
  File "<frozen importlib._bootstrap>", line 957, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 664, in _load_unlocked
  File "<frozen importlib._bootstrap>", line 634, in _load_backward_compatible
  File "/home/odoo11/src/odoo/odoo/modules/module.py", line 82, in load_module
    exec(open(modfile, 'rb').read(), new_mod.__dict__)
  File "<string>", line 4, in <module>
  File "/home/odoo11/src/odoo/addons/base_vat_autocomplete/models/__init__.py", line 4, in <module>
    from . import res_partner
  File "/home/odoo11/src/odoo/addons/base_vat_autocomplete/models/res_partner.py", line 17, in <module>
    stdnum_vat.country_codes = stdnum_vat._country_codes
AttributeError: module 'stdnum.eu.vat' has no attribute '_country_codes'
2021-06-16 08:40:22,381 2321422 CRITICAL grainesdevie-test odoo.service.server: Failed to initialize database `grainesdevie-test`.
Traceback (most recent call last):
  File "/home/odoo11/src/odoo/odoo/service/server.py", line 1054, in preload_registries
    registry = Registry.new(dbname, update_module=update_module)
  File "/home/odoo11/src/odoo/odoo/modules/registry.py", line 85, in new
    odoo.modules.load_modules(registry._db, force_demo, status, update_module)
  File "/home/odoo11/src/odoo/odoo/modules/loading.py", line 379, in load_modules
    force, status, report, loaded_modules, update_module, models_to_check)
  File "/home/odoo11/src/odoo/odoo/modules/loading.py", line 274, in load_marked_modules
    perform_checks=perform_checks, models_to_check=models_to_check
  File "/home/odoo11/src/odoo/odoo/modules/loading.py", line 137, in load_module_graph
    load_openerp_module(package.name)
  File "/home/odoo11/src/odoo/odoo/modules/module.py", line 368, in load_openerp_module
    __import__('odoo.addons.' + module_name)
  File "<frozen importlib._bootstrap>", line 968, in _find_and_load
  File "<frozen importlib._bootstrap>", line 957, in _find_and_load_unlocked
  File "<frozen importlib._bootstrap>", line 664, in _load_unlocked
  File "<frozen importlib._bootstrap>", line 634, in _load_backward_compatible
  File "/home/odoo11/src/odoo/odoo/modules/module.py", line 82, in load_module
    exec(open(modfile, 'rb').read(), new_mod.__dict__)
  File "<string>", line 4, in <module>
  File "/home/odoo11/src/odoo/addons/base_vat_autocomplete/models/__init__.py", line 4, in <module>
    from . import res_partner
  File "/home/odoo11/src/odoo/addons/base_vat_autocomplete/models/res_partner.py", line 17, in <module>
    stdnum_vat.country_codes = stdnum_vat._country_codes
AttributeError: module 'stdnum.eu.vat' has no attribute '_country_codes'
```

Fix:
```sh
su - odoo11
source ~/venv/bin/activate
pip install --force-reinstall python-stdnum==1.14
```
