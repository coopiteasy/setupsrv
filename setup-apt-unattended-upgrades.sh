#!/bin/bash

set -e

apt install --yes unattended-upgrades

# Configure apt unattended upgrades
sed --in-place /etc/apt/apt.conf.d/50unattended-upgrades \
    -e 's/\/*Unattended-Upgrade::Automatic-Reboot .*$/Unattended-Upgrade::Automatic-Reboot "true";/g'
sed --in-place /etc/apt/apt.conf.d/50unattended-upgrades \
    -e 's/\/*Unattended-Upgrade::Automatic-Reboot-WithUsers .*$/Unattended-Upgrade::Automatic-Reboot-WithUsers "false";/g'
sed --in-place /etc/apt/apt.conf.d/50unattended-upgrades \
    -e 's/\/*Unattended-Upgrade::Automatic-Reboot-Time .*$/Unattended-Upgrade::Automatic-Reboot-Time "03:00";/g'
sed --in-place /etc/apt/apt.conf.d/50unattended-upgrades \
    -e 's/\/*Unattended-Upgrade::Mail .*$/Unattended-Upgrade::Mail "root";/g'
sed --in-place /etc/apt/apt.conf.d/50unattended-upgrades \
    -e 's/\/*Unattended-Upgrade::MailReport .*$/Unattended-Upgrade::MailReport "on-change";/g'

# Activate Unattended upgrades
if [ -e /etc/apt/apt.conf.d/20auto-upgrades ]; then
    sed --in-place /etc/apt/apt.conf.d/20auto-upgrades \
        -e 's/\/*APT::Periodic::Update-Package-Lists .*$/APT::Periodic::Update-Package-Lists "1";/g'
    sed --in-place /etc/apt/apt.conf.d/20auto-upgrades \
        -e 's/\/*APT::Periodic::Unattended-Upgrade .*$/APT::Periodic::Unattended-Upgrade "1";/g'
else
    echo "APT::Periodic::Update-Package-Lists \"1\";
APT::Periodic::Unattended-Upgrade \"1\";" > /etc/apt/apt.conf.d/20auto-upgrades
fi
