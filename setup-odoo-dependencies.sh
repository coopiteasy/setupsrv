#!/bin/bash

# Quit if a command fails
set -e

PROG=$(basename "$0")

_usage="Usage: ${PROG} [-h] [-u UBUNTUVER]"
_help="${_usage}

Install odoo dependencies based on ubuntu version given by the nickname
of the version (bionic, focal, etc). Ubuntu version will be guessed if
not provided.

Options:
-h                    show this help text and exit.
-u UBUNTUVER          Ubuntu version (e.g. bionic, focal, etc)."


_ubuntuver=$(lsb_release --codename --short)

SUPPORTED_UBUNTUVER=("bionic" "focal")
SUPPORTED_ODOOVER=("9" "11" "12")

while getopts ':hu:' option; do
    case "$option" in
        h)
            echo "${_help}"
            exit 0
            ;;
        u)
            _ubuntuver=${OPTARG}
            ;;
        \?) ;;
    esac
done
shift $((OPTIND - 1))

if [ -z "${_ubuntuver}" ]; then
    echo "Error: option -u is mandatory." >&2
    echo "${_usage}" >&2
    exit 1
fi

# This working directory should correspond to the root where this script
# stand.
WD=$(dirname "$(realpath -s "$0")")

# Install Postgresql
apt install --yes postgresql

# Install fonts
apt install --yes fonts-liberation

# Install pythons versions
add-apt-repository --yes ppa:deadsnakes/ppa
if [ "${_ubuntuver}" = "bionic" ]; then
    apt install --yes python virtualenv python-virtualenv python-dev python-pip python3 python3-venv python3-dev python3.5 python3.5-venv python3.5-dev python3-pip
elif [ "${_ubuntuver}" = "focal" ]; then
    # Installation of python3-pip will install pip for python3 and python3.7
    apt install --yes python2 python2-dev python3 python3-venv python3-dev virtualenv python3.5 python3.5-dev python3.5-venv python3.6 python3.6-dev python3.6-venv python3-pip
    # Install pip for python2
    curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py
    python2 get-pip.py
    rm get-pip.py
fi

# Install odoo python dependencies devel requirements
apt install --yes postgresql-server-dev-all libpng-dev libjpeg-dev zlib1g-dev libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev

# Dependencies needed on raspberrypi
apt install --yes libopenjp2.7 || true

# Install node-less
apt install --yes node-less

# Install wkhtmltopdf
wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.${_ubuntuver}_amd64.deb
apt install --yes ./wkhtmltox_0.12.5-1.${_ubuntuver}_amd64.deb
rm wkhtmltox_0.12.5-1.${_ubuntuver}_amd64.deb
