#!/bin/bash

# Quit if a command fails
set -e

# This working directory should correspond to the root where this script
# stand.
WD=$(dirname "$(realpath -s "$0")")

cat "$WD/files/bash.bashrc" >> /etc/bash.bashrc

# We do not save /etc/skel/.bashrc as it will copied in each new home
# created after.
rm /etc/skel/.bashrc
cp "$WD/files/bashrc" /etc/skel/.bashrc

mv /root/.bashrc /root/.bashrc.bak.$(date --iso-8601)
cp "$WD/files/bashrc" /root/.bashrc
