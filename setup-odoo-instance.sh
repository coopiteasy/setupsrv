#!/bin/bash

# Quit if a command fails
set -e

PROG=$(basename "$0")

_usage="Usage: ${PROG} [-h] -e ENV -r INVREPOURL ODOOINSTANCE"
_help="${_usage}

Install odoo from configuration files in INVREPOURL as ODOOINSTANCE.

Recommended python version:
- odoo 9.0:  python2.7
- odoo 10.0: python2.7
- odoo 11.0: python3.5
- odoo 12.0: python3.6
- odoo 13.0: python3.6
- odoo 14.0: python3.8

Options:
-h                    show this help text and exit.
-e ENV                name of the enviroment (e.g. prod, staging, test).
                      [Default: prod]
-p PYTHONEXE          path to python executable. [Default: python3]
-r INVREPOURL         URL to the gitlab repository containing
                      configuration files (the inventory repo)."


_invrepourl=""
_python="python3"
_env="prod"

while getopts ':he:p:r:' option; do
    case "$option" in
        h)
            echo "${_help}"
            exit 0
            ;;
        e)
            _env=${OPTARG}
            ;;
        p)
            _python=${OPTARG}
            ;;
        r)
            _invrepourl=${OPTARG}
            ;;
        \?) ;;
    esac
done
shift $((OPTIND - 1))

ODOOINSTANCE=$1
ODOOHOME=/home/${ODOOINSTANCE}
ODOOSRC="${ODOOHOME}/src"
ODOOVENV="${ODOOHOME}/venv"
ODOOBIN="odoo"  # This is the default, it can be overwritten by variable in INSTANCECONF
INVDIR="${ODOOHOME}/inventory"
INSTANCECONF="${INVDIR}/instance-${_env}.conf"

# This working directory should correspond to the root where this script
# stand.
WD=$(dirname "$(realpath -s "$0")")

if [ ! $# -eq 1 ]; then
    echo "Error: $0 takes 1 positional argument, but $# given." >&2
    echo "${_usage}" >&2
    exit 1
fi

if ! id -u ${ODOOINSTANCE} >/dev/null; then
    echo "Error: user name '$1' does not exists." >&2
    exit 1
fi

# Clone inventory repository
if [ -d "${INVDIR}" ]; then
    rm -rf "${INVDIR}"
fi
sudo --set-home -u ${ODOOINSTANCE} git clone "${_invrepourl}" "${INVDIR}"

# Load configuration file
# Executing a configuration file can be dangerous, as malicious people
# can write commands in the configuration file. So, this configuration
# file should be secured as .bashrc file is.
if [ ! -f "${INSTANCECONF}" ]; then
    echo "Error: No configuration file in the inventory repository." >&2
    exit 2
fi
source "${INSTANCECONF}"

# Check that configuration variable are correctly sets
if [ -z "${ODOOCONFNAME}" ]; then
    echo "Error: Missing ODOOCONFNAME key in ${INSTANCECONF}." >&2
    exit 3
fi
if [ -z "${GAREPOURL}" ]; then
    echo "Error: Missing GAREPOURL key in ${INSTANCECONF}." >&2
    exit 3
fi
if [ -z "${GADIRNAME}" ]; then
    echo "Error: Missing GADIRNAME key in ${INSTANCECONF}." >&2
    exit 3
fi
if [ -z "${GACONFNAME}" ]; then
    echo "Error: Missing GACONFNAME key in ${INSTANCECONF}." >&2
    exit 3
fi

ODOOCONF="${INVDIR}/${ODOOCONFNAME}"
GACONF="${ODOOHOME}/${GADIRNAME}/${GACONFNAME}"

# Clone git-aggregator configuration repository
if [ -d "${ODOOHOME}/${GADIRNAME}" ]; then
    rm -rf "${ODOOHOME}/${GADIRNAME}"
fi
sudo --set-home -u ${ODOOINSTANCE} git clone "${GAREPOURL}" "${ODOOHOME}/${GADIRNAME}"

# Get port numbers
_httpport=$(cat "${ODOOCONF}" | grep http_port | head -n 1 | sed -e 's/[ \t]//g' | sed -e 's/http_port=//')
_longport=$(cat "${ODOOCONF}" | grep longpolling_port | head -n 1 | sed -e 's/[ \t]//g' | sed -e 's/longpolling_port=//')
# Ensure default value for ports
_httpport=${_httpport:-"8069"}
_longport=${_longport:-"8072"}


# Create symlink
rm ${ODOOHOME}/odoo.conf ${ODOOHOME}/repos.yml || true
rm -rf ${ODOOSRC} || true
sudo --set-home -u ${ODOOINSTANCE} ln -s "${ODOOCONF}" ${ODOOHOME}/odoo.conf
sudo --set-home -u ${ODOOINSTANCE} ln -s "${GACONF}" ${ODOOHOME}/repos.yml
sudo --set-home -u ${ODOOINSTANCE} mkdir "${ODOOSRC}"

cd ${ODOOSRC}

# Get code
sudo --set-home -u ${ODOOINSTANCE} gitaggregate -c ${ODOOHOME}/repos.yml

# Create virtualenv
sudo --set-home -u ${ODOOINSTANCE} virtualenv --python ${_python} "${ODOOVENV}"

source "${ODOOVENV}/bin/activate"

# Install requirements
sudo --set-home -u ${ODOOINSTANCE} find -L ${ODOOHOME} -name "requirements.txt" -exec "${ODOOVENV}/bin/python" -m pip install --force-reinstall -r {} \;

# Install odoo in the virtualenv
sudo --set-home -u ${ODOOINSTANCE} "${ODOOVENV}/bin/python" -m pip install -e "${ODOOSRC}/odoo"

# Configure deamon
cp "${WD}/templates/odoo.service" /etc/systemd/system/${ODOOINSTANCE}.service
sed -i -e 's/{{odooinstance}}/'${ODOOINSTANCE}'/g' /etc/systemd/system/${ODOOINSTANCE}.service
sed -i -e 's#{{odoo-bin-path}}#'${ODOOVENV}'/bin/'${ODOOBIN}'#g' /etc/systemd/system/${ODOOINSTANCE}.service
systemctl daemon-reload
systemctl enable ${ODOOINSTANCE}

# Sudo access
sed -e 's/{{odooinstance}}/'${ODOOINSTANCE}'/g' "${WD}/templates/sudoers" \
    | EDITOR=tee visudo -f /etc/sudoers.d/${ODOOINSTANCE}

# Nginx instance config
cp "${WD}/templates/default-nginx" /etc/nginx/sites-available/${ODOOINSTANCE}-srv
sed -i -e 's/{{odooinstance}}/'${ODOOINSTANCE}'/g' /etc/nginx/sites-available/${ODOOINSTANCE}-srv
sed -i -e 's/{{httpport}}/'${_httpport}'/g' /etc/nginx/sites-available/${ODOOINSTANCE}-srv
sed -i -e 's/{{longport}}/'${_longport}'/g' /etc/nginx/sites-available/${ODOOINSTANCE}-srv
rm /etc/nginx/sites-enabled/${ODOOINSTANCE}-srv || true
ln -s /etc/nginx/sites-available/${ODOOINSTANCE}-srv /etc/nginx/sites-enabled/

nginx -t
systemctl reload nginx

# Create nginx template for this instance
cp "${WD}/templates/nginx-instance-template" /etc/nginx/sites-available/template-${ODOOINSTANCE}
sed -i -e 's/{{odooinstance}}/'${ODOOINSTANCE}'/g' /etc/nginx/sites-available/template-${ODOOINSTANCE}


# Configure ociedoo
sudo --set-home -u ${ODOOINSTANCE} mkdir -p ${ODOOHOME}/.config/ociedoo
cp "${WD}/templates/ociedoo.conf" ${ODOOHOME}/.config/ociedoo/ociedoorc
chown --recursive ${ODOOINSTANCE}:${ODOOINSTANCE} ${ODOOHOME}/.config/ociedoo
sudo --set-home -u ${ODOOINSTANCE} sed -i -e 's/{{odooinstance}}/'${ODOOINSTANCE}'/g' ${ODOOHOME}/.config/ociedoo/ociedoorc


# Run custom-installation if one exists
if [ -x "${INVDIR}/custom-install" ]; then
    ${INVDIR}/custom-install ${ODOOINSTANCE}
fi

exit 0
