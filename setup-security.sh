#!/bin/bash

# Quit if a command fails
set -e

# This working directory should correspond to the root where this script
# stand.
WD=$(dirname "$(realpath -s "$0")")

# SSH
mv /etc/ssh/sshd_config "/etc/ssh/ssh_config.bak.$(date --iso-8601)"
cp "$WD/files/sshd_config" /etc/ssh/

# Fail2ban

apt install fail2ban --yes

cp "$WD/files/fail2ban" /etc/fail2ban/jail.local

systemctl restart fail2ban

# Check sshd and restart ssh
sshd -t
systemctl restart ssh
