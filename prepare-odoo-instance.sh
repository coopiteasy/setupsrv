#!/bin/bash

# Quit if a command fails
set -e


_usage="Usage: $0 [-h] ODOOINSTANCE"
_help="${_usage}

Create odoo instance named ODOOINSTANCE.
It will create a new user and configure it correctly.

It will NOT install odoo.

Options:
-h                    show this help text and exit."

while getopts ':h' option; do
    case "$option" in
        h)
            echo "${_help}"
            exit 0
            ;;
        \?) ;;
    esac
done
shift $((OPTIND - 1))

ODOOINSTANCE=$1

if [ ! $# -eq 1 ]; then
    echo "Error: $0 takes 1 positional argument, but $# given." >&2
    echo "${_usage}" >&2
    exit 1
fi

if id -u ${ODOOINSTANCE} >/dev/null; then
    echo "Error: user name '$1' already exists." >&2
    exit 1
fi

# This working directory should correspond to the root where this script
# stand.
WD=$(dirname "$(realpath -s "$0")")

yes "" | adduser ${ODOOINSTANCE}

# Create postgresql user and do not fail if this user already exists
sudo -u postgres createuser -d ${ODOOINSTANCE} || true

yes "" | sudo --set-home -u ${ODOOINSTANCE} ssh-keygen -t ed25519 -N ""

cd /home/${ODOOINSTANCE}
sudo --set-home -u ${ODOOINSTANCE} git config --global user.email "serveurs@coopiteasy.be"
sudo --set-home -u ${ODOOINSTANCE} git config --global user.name "infra-cie"
sudo --set-home -u ${ODOOINSTANCE} git config --global init.defaultBranch master
sudo --set-home -u ${ODOOINSTANCE} git config --global pull.rebase false
cd ${WD}
