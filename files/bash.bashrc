# Configure fancy prompt
# ======================

prompt_env=TEST
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    if [ "${prompt_env}" = PROD ]; then
        PS1='
\[\e[0;31m\]'${prompt_env}' \h\[\e[00m\] : \[\e[0;34m\]\w\[\e[00m\]'
    elif [ "${prompt_env}" = STAGING ]; then
        PS1='
\[\e[0;33m\]'${prompt_env}' \h\[\e[00m\] : \[\e[0;34m\]\w\[\e[00m\]'
    else
        PS1='
\[\e[0;32m\]'${prompt_env}' \h\[\e[00m\] : \[\e[0;34m\]\w\[\e[00m\]'
    fi

    if [ "$USER" = root ]; then
        PS1=${PS1}'
\[\e[0;31m\]\u\[\e[00m\] \$ '
    else
        PS1=${PS1}'
\[\e[0;36m\]\u\[\e[00m\] \$ '
    fi
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt prompt_env

# LANG
# ====
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

# ociedoo
# =======
if command -v ociedoo >/dev/null; then
    eval "$(_OCIEDOO_COMPLETE=source ociedoo)"
fi
