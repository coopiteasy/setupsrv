#!/bin/bash

# Quit if a command fails
set -e

# This working directory should correspond to the root where this script
# stand.
WD=$(dirname "$(realpath -s "$0")")

# Git repository (to have last version of git)
sudo add-apt-repository --yes ppa:git-core/ppa
sudo apt update

# Main tools
apt install --yes git python3-pip python3-venv htop

# cie-scripts
mkdir -p /usr/share/cie
if [ ! -d /usr/share/cie/scripts ]; then
    git clone https://gitlab.com/coopiteasy/scripts /usr/share/cie/scripts
    ln -s /usr/share/cie/scripts/bin /usr/ciebin || true
else
    echo "INFO: cie-script seams to be already installed."
fi

# pipx
python3 -m pip install pipx

# pgcli
sudo PIPX_HOME=/usr PIPX_BIN_DIR=/usr/bin pipx install pgcli

# ociedoo
apt install --yes libfile-mimeinfo-perl
sudo PIPX_HOME=/usr PIPX_BIN_DIR=/usr/bin pipx install git+https://gitlab.com/coopiteasy/ociedoo#egg=ociedoo

# git-aggregator
sudo PIPX_HOME=/usr PIPX_BIN_DIR=/usr/bin pipx install git-aggregator

# ripgrep
wget https://github.com/BurntSushi/ripgrep/releases/download/12.1.1/ripgrep_12.1.1_amd64.deb
dpkg -i ripgrep_12.1.1_amd64.deb
rm ripgrep_12.1.1_amd64.deb

# fdfind
wget https://github.com/sharkdp/fd/releases/download/v8.2.1/fd_8.2.1_amd64.deb
dpkg -i fd_8.2.1_amd64.deb
rm fd_8.2.1_amd64.deb
