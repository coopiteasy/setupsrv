#!/bin/bash

set -e

apt install --yes rsync

# Create backup dir
mkdir -p /backup

# Create users
useradd --no-create-home --home-dir /backup --system --shell /bin/bash rbackup
useradd --groups rbackup --no-create-home --no-user-group --system --shell /sbin/nologin sbackup

# Create authorized_keys files
mkdir -p /etc/ssh/authorized_keys/rbackup
touch /etc/ssh/authorized_keys/rbackup/authorized_keys
mkdir -p /etc/ssh/authorized_keys/sbackup
touch /etc/ssh/authorized_keys/sbackup/authorized_keys

# Configure SSH
sed --in-place /etc/ssh/sshd_config \
    -e 's/#*AuthorizedKeysFile.*$/AuthorizedKeysFile      %h\/.ssh\/authorized_keys \/etc\/ssh\/authorized_keys\/%u\/authorized_keys/g'
sed --in-place /etc/ssh/sshd_config \
    -e 's/#*MaxStartups.*$/MaxStartups 10:30:60/g'
sed --in-place /etc/ssh/sshd_config \
    -e 's/#*PermitEmptyPasswords.*$/PermitEmptyPasswords no/g'


echo "
# Configuration for rsync access
Match User rbakcup
    PermitTunnel no
    AllowAgentForwarding no
    AllowTcpForwarding no
    X11Forwarding no
    PermitTTY no

# Configuration for sftp access
Match User sbackup
    ChrootDirectory /backup
    ForceCommand internal-sftp
    PermitTunnel no
    X11Forwarding no
    AllowTcpForwarding no" \
    >> /etc/ssh/sshd_config

sshd -t

service sshd reload

# Create cron task
echo "* * * * * root /bin/chmod -R 750 /backup/*
* * * * * root /bin/chown -R rbackup:rbackup /backup/*
" > /etc/cron.d/ensure-backup-permission

touch /etc/cron.d/cleanbu
